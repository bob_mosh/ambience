//
//  ListStyleModifier.swift
//  Ambience
//
//  Created by Ferdinand Göldner on 21.02.23.
//

import SwiftUI

struct PlainListStyleModifier: ViewModifier {
    func body(content: Content) -> some View {
        content
            .listRowInsets(.none)
            .listRowBackground(Color.clear)
            .listRowSeparator(.hidden)
    }
}

public extension View {
    func listRowPlain() -> some View {
        modifier(PlainListStyleModifier())
    }
}

struct ListStyleBoxedModifier: ViewModifier {
    var isActive: Bool

    func body(content: Content) -> some View {
        content
            .listRowBackground(
                ZStack {
                    isActive ?
                        Color.accentColor.opacity(0.3) :
                        Color.primary.opacity(0.05)
                }
                .cornerRadius(8.0)
                .padding(.vertical, 4.0)
            )
            .listRowPlain()
    }
}

public extension View {
    func listRowBoxed(isActive: Bool) -> some View {
        modifier(ListStyleBoxedModifier(isActive: isActive))
    }
}
