//
//  Loop.swift
//  Ambience
//
//  Created by Ferdinand Göldner on 23.02.23.
//

import Foundation

struct Loop: Identifiable, Equatable, Hashable {
    var id: String { file }
    var title: String
    var file: String
}
