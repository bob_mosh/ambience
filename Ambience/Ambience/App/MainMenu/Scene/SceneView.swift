//
//  SceneView.swift
//  Ambience
//
//  Created by Ferdinand Göldner on 20.02.23.
//

import SwiftUI
import ComposableArchitecture

struct SceneView: View {
    @Environment(\.horizontalSizeClass) var horizontalSizeClass
    
    let store: Store<SceneCore.State, SceneCore.Action>
    @ObservedObject var viewStore: ViewStore<SceneCore.State, SceneCore.Action>
    
    init(store: Store<SceneCore.State, SceneCore.Action>) {
        self.store = store
        self.viewStore = ViewStore(store)
    }
    
    var gridItems: [GridItem] {
        get {
            if horizontalSizeClass == .compact {
                return [.init()]
            } else {
                return [.init(), .init(), .init()]
            }
        }
    }
    
    var body: some View {
        ScrollView {
            LazyVGrid(columns: gridItems) {
                ForEachStore(
                    store.scope(
                        state: \.presets,
                        action: SceneCore.Action.preset
                    )
                ) { store in
                    PresetView(store: store)
                }
            }
            .padding()
            
            Divider()
            
            LazyVGrid(columns: gridItems) {
                ForEachStore(
                    store.scope(
                        state: \.shots,
                        action: SceneCore.Action.shot
                    )
                ) { store in
                    OneShotView(store: store)
                }
            }
            .padding()
            
            Divider()
            
            LazyVGrid(columns: gridItems) {
                ForEachStore(
                    store.scope(
                        state: \.loops,
                        action: SceneCore.Action.loop
                    )
                ) { store in
                    LoopView(store: store)
                }
            }
            .padding()
        }
        .navigationTitle(viewStore.scene.title)
        .toolbar {
            ToolbarItem(placement: .navigationBarLeading) {
                AirPlayView()
            }
        }
    }
}
