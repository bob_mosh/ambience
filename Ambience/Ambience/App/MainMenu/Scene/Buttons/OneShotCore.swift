//
//  OneShotCore.swift
//  Ambience
//
//  Created by Ferdinand Göldner on 27.02.23.
//

import ComposableArchitecture

struct OneShotCore: ReducerProtocol {
    // MARK: Public

    public static func store(shot: Shot) -> Store<State, Action> {
        return .init(initialState: .init(shot: shot),
                     reducer: OneShotCore())
    }
    
    // MARK: Internal

    struct State: Equatable, Identifiable, Hashable {
        var id: String { shot.id }
        var shot: Shot
    }
    
    enum Action {
        case playShot
    }

    @Dependency(\.shotManager) var shotManager

    var body: some ReducerProtocol<State, Action> {
        Reduce { state, action in
            switch action {
            case .playShot:
                guard let variant = state.shot.variants.randomElement() else { return .none }
                shotManager.playSound(withName: variant)
            }
            return .none
        }
    }
}
