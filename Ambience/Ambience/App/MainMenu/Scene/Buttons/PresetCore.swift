//
//  PresetCore.swift
//  Ambience
//
//  Created by Ferdinand Göldner on 21.02.23.
//

import ComposableArchitecture
import Foundation

struct PresetCore: ReducerProtocol {
    struct State: Equatable, Hashable, Identifiable {
        var id: UUID { preset.id }
        var preset: Preset
    }
    
    enum Action {
        case startPreset
    }
    
    @Dependency(\.ambientManager) var ambientManager
    
    public static func store(preset: Preset) -> Store<State, Action> {
        return  .init(initialState: .init(preset: preset),
                      reducer: PresetCore())
    }
    
    var body: some ReducerProtocol<State, Action> {
        Reduce { state, action in
            switch action {
            case .startPreset:
                ambientManager.load(preset: state.preset)
            }
            return .none
        }
    }
}
