//
//  SceneCore.swift
//  Ambience
//
//  Created by Ferdinand Göldner on 20.02.23.
//

import ComposableArchitecture
import Foundation

struct SceneCore: ReducerProtocol {
    // MARK: Public

    public static func store(scene: AmbientScene) -> Store<State, Action> {
        return .init(initialState: .init(scene: scene),
                     reducer: SceneCore())
    }
    
    // MARK: Internal

    struct State: Equatable, Hashable, Identifiable {
        // MARK: Lifecycle
        var id: UUID { scene.id }

        init(scene: AmbientScene) {
            self.scene = scene
            
            self.presets = IdentifiedArrayOf<PresetCore.State>(
                uniqueElements:
                scene.presets.map { PresetCore.State(preset: $0) }
            )
            
            self.shots = IdentifiedArrayOf<OneShotCore.State>(
                uniqueElements:
                scene.oneshots.map { OneShotCore.State(shot: $0) }
            )
            
            self.loops = IdentifiedArrayOf<LoopCore.State>(
                uniqueElements:
                scene.loops.map { LoopCore.State(loop: $0) }
            )
        }
        
        // MARK: Internal

        var scene: AmbientScene
        
        var presets: IdentifiedArrayOf<PresetCore.State>
        var shots: IdentifiedArrayOf<OneShotCore.State>
        var loops: IdentifiedArrayOf<LoopCore.State>
    }
    
    enum Action {
        case sceneWillLoad
        case stopAllSounds
        
        case preset(UUID, PresetCore.Action)
        case shot(String, OneShotCore.Action)
        case loop(String, LoopCore.Action)
    }
    
    @Dependency(\.ambientManager) var ambientManager

    var body: some ReducerProtocol<State, Action> {
        Reduce { state, action in
            switch action {
            case .sceneWillLoad:
                var effects: [EffectTask<Action>] = Loop.allLoops.map { .send(.loop($0.id, .load)) }
                return .concatenate(effects)
                
            case .shot: break
                
            case .loop: break
                
            case .stopAllSounds:
                var effects: [EffectTask<Action>] = Loop.allLoops.map { .send(.loop($0.id, .stopSound)) }
                return .concatenate(effects)
                
            case .preset: break
            }
            return .none
        }
        .forEach(\.presets, action: /Action.preset) {
            PresetCore()
        }
        .forEach(\.shots, action: /Action.shot) {
            OneShotCore()
        }
        .forEach(\.loops, action: /Action.loop) {
            LoopCore()
        }
    }
}
