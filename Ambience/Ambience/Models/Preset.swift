//
//  Preset.swift
//  Ambience
//
//  Created by Ferdinand Göldner on 23.02.23.
//

import Foundation

struct Preset: Identifiable, Equatable, Hashable {
    var id: UUID
    var title: String
    var settings: [Setting]
    
    struct Setting: Equatable, Hashable {
        var loop: Loop
        var volume: Float
    }
}
