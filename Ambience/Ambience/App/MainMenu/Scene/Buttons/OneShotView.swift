//
//  OneShotView.swift
//  Ambience
//
//  Created by Ferdinand Göldner on 27.02.23.
//

import ComposableArchitecture
import SwiftUI

struct OneShotView: View {
    // MARK: Lifecycle

    init(store: Store<OneShotCore.State, OneShotCore.Action>) {
        self.store = store
        self.viewStore = ViewStore(store)
    }

    // MARK: Internal

    let store: Store<OneShotCore.State, OneShotCore.Action>
    @ObservedObject var viewStore: ViewStore<OneShotCore.State, OneShotCore.Action>

    var body: some View {
        Button {
            viewStore.send(.playShot)
        } label: {
            ZStack {
                HStack {
                    Text(viewStore.shot.title)
                        .fontWeight(.bold)
                    
                    Spacer()
                }
                .frame(maxWidth: .infinity)
                .padding()
            }
            .background(
                Color.primary.opacity(0.05)
            )
            .cornerRadius(8.0)
        }
        .foregroundColor(.primary)
    }
}
