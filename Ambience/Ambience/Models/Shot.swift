//
//  OneShot.swift
//  Ambience
//
//  Created by Ferdinand Göldner on 23.02.23.
//

import Foundation

struct Shot: Identifiable, Equatable, Hashable {
    var id: String { title }
    var title: String
    var variants: [String]
    
    init(title: String, numberofVariants: Int) {
        self.title = title
        self.variants = (1...numberofVariants).map({ index in
            "\(title) \(index)"
        })
    }
}
