//
//  AmbienceAppView.swift
//  Ambience
//
//  Created by Ferdinand Göldner on 20.02.23.
//

import SwiftUI
import ComposableArchitecture
import AVFAudio

@main
struct AmbienceApp: App {
    init() {
        self.init(
            store: AmbienceAppCore.store()
        )
    }

    let store: Store<AmbienceAppCore.State, AmbienceAppCore.Action>
    @ObservedObject var viewStore: ViewStore<AmbienceAppCore.State, AmbienceAppCore.Action>

    init(store: Store<AmbienceAppCore.State, AmbienceAppCore.Action>) {
        self.store = store
        self.viewStore = ViewStore(store)
    }

    var body: some Scene {
        WindowGroup {
            MainMenuView(
                store: store.scope(
                    state: \.mainMenu,
                    action: AmbienceAppCore.Action.mainMenu
                )
            )
        }
    }
}
