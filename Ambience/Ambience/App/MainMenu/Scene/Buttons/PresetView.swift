//
//  PresetView.swift
//  Ambience
//
//  Created by Ferdinand Göldner on 21.02.23.
//

import SwiftUI
import ComposableArchitecture

struct PresetView: View {
    let store: Store<PresetCore.State, PresetCore.Action>
    @ObservedObject var viewStore: ViewStore<PresetCore.State, PresetCore.Action>
    
    init(store: Store<PresetCore.State, PresetCore.Action>) {
        self.store = store
        self.viewStore = ViewStore(store)
    }
    
    var body: some View {
        Button {
            viewStore.send(.startPreset)
        } label: {
            ZStack {
                HStack {
                    Text(viewStore.preset.title)
                        .fontWeight(.bold)
                    
                    Spacer()
                }
                .frame(maxWidth: .infinity)
                .padding()
            }
            .background(
                Color.primary.opacity(0.05)
            )
            .cornerRadius(8.0)
        }
        .foregroundColor(.primary)
    }
}
