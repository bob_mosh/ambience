//
//  AmbientScene.swift
//  Ambience
//
//  Created by Ferdinand Göldner on 23.02.23.
//

import Foundation

struct AmbientScene: Identifiable, Equatable, Hashable {
    var id: UUID
    var title: String
    
    var presets: [Preset] = []
    var loops: [Loop] = []
    var oneshots: [Shot] = Shot.allShots
    
    static var greenest: AmbientScene {
        AmbientScene(id: .init(), title: "Greenest",
        presets: [
            .init(id: .init(),
                  title: "Happy Town",
                  settings: [
                    .init(loop: .marketPeople, volume: 0.3),
                    .init(loop: .footsteps, volume: 0.4),
                    .init(loop: .tavernNoises, volume: 0.2),
                    .init(loop: .birds, volume: 0.1),
                  ]),
            .init(id: .init(),
                  title: "Nighttime at the Stream",
                  settings: [
                    .init(loop: .nighttimeBugs, volume: 0.2),
                    .init(loop: .seasideDocks, volume: 0.4)
                  ]),
            .init(id: .init(),
                  title: "Nighttime inside the Tower",
                  settings: [
                    .init(loop: .footsteps, volume: 0.2),
                    .init(loop: .marketPeople, volume: 0.1),
                    .init(loop: .campfireClose, volume: 1.0),
                    .init(loop: .distantThunderstormWithLightRain, volume: 0.2)
                  ]),
            .init(id: .init(),
                  title: "Attack",
                  settings: [
                    .init(loop: .thunderstorm, volume: 1.0),
                    .init(loop: .caveMonsters, volume: 0.3),
                    .init(loop: .roaringFire, volume: 0.4)
                  ])
            
        ], loops: Loop.allLoops)
    }
    
    static var forrestWalk: AmbientScene {
        AmbientScene(id: .init(), title: "Forrest Walk",
        presets: [
            .init(id: .init(),
                  title: "Walking in the Forrest",
                  settings: [
                    .init(loop: .footsteps, volume: 0.6),
                    .init(loop: .bugs, volume: 0.2),
                    .init(loop: .birds, volume: 0.3),
                  ]),
            .init(id: .init(),
                  title: "Nighttime Camp",
                  settings: [
                    .init(loop: .nighttimeBugs, volume: 0.4),
                    .init(loop: .campfireClose, volume: 1.0),
                  ])
        ], loops: Loop.allLoops.dropLast())
    }
}
