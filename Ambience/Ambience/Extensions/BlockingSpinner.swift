//
//  BlockingSpinner.swift
//  Ambience
//
//  Created by Ferdinand Göldner on 21.02.23.
//

import SwiftUI

struct BlockingSpinner: ViewModifier {
    var isActive: Bool

    func body(content: Content) -> some View {
        content
            .overlay {
                if isActive {
                    ZStack {
                        Rectangle()
                            .foregroundColor(Color.black.opacity(0.4))
                        ProgressView()
                    }.edgesIgnoringSafeArea(.all)
                }
            }
    }
}

extension View {
    func blocked(active: Bool) -> some View {
        modifier(BlockingSpinner(isActive: active))
    }
}
