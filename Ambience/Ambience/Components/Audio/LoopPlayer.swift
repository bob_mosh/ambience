//
//  AudioPlayer.swift
//  Ambience
//
//  Created by Ferdinand Göldner on 23.02.23.
//

import AVFAudio
import Combine

class LoopPlayer: AVAudioPlayer, Identifiable {
    var id: String
    
    required init?(filename: String) {
        self.id = filename
        self.volumeSubject = .init(0.0)
        self.isPlayingSubject = .init(false)
        
        guard let path = Bundle.main.path(forResource: "\(filename).mp3", ofType: nil) else { return nil }
        let url = URL(fileURLWithPath: path)

        do {
            try super.init(contentsOf: url, fileTypeHint: nil)
            self.volume = 0.0
            self.numberOfLoops = -1
        } catch {
            print(error)
            return nil
        }
    }
    
    override var volume: Float {
        get { super.volume }
        set { setVolume(newValue, fadeDuration: 0.0) }
    }
    
    override func setVolume(_ volume: Float, fadeDuration duration: TimeInterval = 2.0) {
        if isPlaying == false && volume != 0.0 { play() }
        
        super.setVolume(volume, fadeDuration: duration)
        volumeSubject.send(volume)
        isPlayingSubject.send(isPlaying)
    }
    
    override func play() -> Bool {
        let result = super.play()
        if volume == 0.0 { setVolume(1.0) }
        return result
    }
    
    func stop(animated: Bool) {
        if animated { setVolume(0.0) }
        else { super.stop() }
        isPlayingSubject.send(false)
    }
    
    var volumeSubject: CurrentValueSubject<Float, Never>
    var isPlayingSubject: CurrentValueSubject<Bool, Never>
}

