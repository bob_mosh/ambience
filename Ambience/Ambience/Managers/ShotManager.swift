//
//  ShotManager.swift
//  Ambience
//
//  Created by Ferdinand Göldner on 27.02.23.
//

import AVFAudio
import Combine
import Dependencies
import IdentifiedCollections

private enum ShotManagerKey: DependencyKey {
    static let liveValue = ShotManager.live
}

extension DependencyValues {
    var shotManager: ShotManager {
        get { self[ShotManagerKey.self] }
        set { self[ShotManagerKey.self] = newValue }
    }
}

class ShotManager {
    // MARK: Internal

    static var live = ShotManager()

    func playSound(withName name: String) {
        guard let shot = ShotPlayer(filename: name) else { return }
        shots.updateOrAppend(shot)
        shot
            .completedSubject()
            .sink { _ in
                self.shots.remove(shot)
            } receiveValue: { _ in }
            .store(in: &cancellables)
    }

    // MARK: Private

    private var shots: IdentifiedArrayOf<ShotPlayer> = []
    private var cancellables: Set<AnyCancellable> = []
}
