//
//  MainMenuCore.swift
//  Ambience
//
//  Created by Ferdinand Göldner on 20.02.23.
//

import Foundation
import ComposableArchitecture

struct MainMenuCore: ReducerProtocol {
    struct State: Equatable {
        var scenes: IdentifiedArrayOf<SceneCore.State> = .init()
        var navigatedScene: SceneCore.State?
//        var activeScene: SceneCore.State?
        
        init() {
            self.scenes = .init(uniqueElements: [
                .greenest,
                .forrestWalk
            ].map({ SceneCore.State(scene: $0) }))
            
//            self.activeScene = scenes.first
        }
    }
    
    enum Action {
        case navigateToScene(SceneCore.State?)
        
//        case activeScene(SceneCore.Action)
        case navigatedScene(SceneCore.Action)
    }
    
    public static func store() -> Store<State, Action> {
        return  .init(initialState: .init(),
                      reducer: MainMenuCore())
    }
    
    var body: some ReducerProtocol<State, Action> {
        Reduce { state, action in
            switch action {
            case let .navigateToScene(scene):
                guard let scene = scene else {
                    state.navigatedScene = nil
                    return .none
                }
                state.navigatedScene = state.scenes[id: scene.id]
                return .send(.navigatedScene(.sceneWillLoad))
                
            case .navigatedScene: break
            }
            
            return .none
        }
        .ifLet(\.navigatedScene, action: /Action.navigatedScene) {
            SceneCore()
        }
    }
}
