//
//  LoopView.swift
//  Ambience
//
//  Created by Ferdinand Göldner on 20.02.23.
//

import ComposableArchitecture
import SwiftUI

struct LoopView: View {
    // MARK: Lifecycle

    init(store: Store<LoopCore.State, LoopCore.Action>) {
        self.store = store
        self.viewStore = ViewStore(store)
    }
    
    @GestureState private var offset: CGSize = .zero
    
    // MARK: Internal

    let store: Store<LoopCore.State, LoopCore.Action>
    @ObservedObject var viewStore: ViewStore<LoopCore.State, LoopCore.Action>

    var body: some View {
        ZStack(alignment: .top) {
            HStack {
                Text(viewStore.loop.title)
                    .fontWeight(.bold)
                
                Spacer()
            }
            .frame(maxWidth: .infinity)
            .padding()
            
            ProgressView(value: viewStore.volume)
                .accentColor(
                    viewStore.isPlaying ?
                    Color.accentColor :
                    Color.primary.opacity(0.6)
                )
        }
        .background(
            viewStore.isPlaying ?
                Color.accentColor.opacity(0.05) :
                Color.primary.opacity(0.05)
        )
        .cornerRadius(8.0)
        .foregroundColor(.primary)
        .gesture(
            ExclusiveGesture(
                TapGesture()
                    .onEnded(
                        { viewStore.send(.soundButtonPressed) }
                    ),
                DragGesture(
                    minimumDistance: 0.0,
                    coordinateSpace: .local
                ).onChanged { value in
                    viewStore.send(.soundButtonDragged(value.translation))
                }.onEnded({ value in
                    viewStore.send(.soundButtonDragEnded(value.translation))
                })
            )
        )
    }
}
