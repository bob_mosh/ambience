//
//  AirPlayView.swift
//  Ambience
//
//  Created by Ferdinand Göldner on 23.02.23.
//

import AVKit
import SwiftUI

struct AirPlayView: UIViewRepresentable {
    func updateUIView(_ uiView: UIView, context: Context) {}
    
    func makeUIView(context: Context) -> UIView {
        let routePickerView = AVRoutePickerView()
        
        // Configure the button's color.
        routePickerView.tintColor = UIColor(.primary)
        routePickerView.activeTintColor = UIColor(named: "accentColor")
        
        // Indicate whether your app prefers video content.
        routePickerView.prioritizesVideoDevices = false
        
        return routePickerView
    }
}
