//
//  AmbienceAppCore.swift
//  Ambience
//
//  Created by Ferdinand Göldner on 20.02.23.
//

import ComposableArchitecture

struct AmbienceAppCore: ReducerProtocol {
    struct State: Equatable {
        var mainMenu: MainMenuCore.State
    }
    
    enum Action {
        case mainMenu(MainMenuCore.Action)
    }
    
    public static func store() -> Store<State, Action> {
        return  .init(
            initialState: .init(
                mainMenu: .init()
            ),
            reducer: AmbienceAppCore())
    }
    
    var body: some ReducerProtocol<State, Action> {
        Scope(state: \.mainMenu, action: /Action.mainMenu) {
            MainMenuCore()
        }
        Reduce { state, action in
            switch action {
            default: return .none
            }
        }
    }
}
