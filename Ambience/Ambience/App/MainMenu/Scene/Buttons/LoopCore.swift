//
//  LoopCore.swift
//  Ambience
//
//  Created by Ferdinand Göldner on 20.02.23.
//

import Foundation
import ComposableArchitecture
import AVFAudio

struct LoopCore: ReducerProtocol {
    struct State: Equatable, Identifiable, Hashable {
        var id: String { loop.id }
        var loop: Loop
        
        var isPlaying: Bool = false
        var isLoading: Bool = false
        var volumeOffset: Float?
        
        var volume: Float = 0.0
        
        init(loop: Loop) {
            self.loop = loop
        }
    }
    
    enum Action {
        // Automatic actions
        case load
        case loadAudioFile
        case toggleSound
        case startSound
        case stopSound
        case updateVolumeOffset(Float)
        case resetOffset
        case setVolume(Float)
        case setIsPlaying(Bool)
        
        // User Interactions
        case soundButtonPressed
        case soundButtonDragged(CGSize)
        case soundButtonDragEnded(CGSize)
        
        case none
    }
    
    @Dependency(\.ambientManager) var ambientManager
    
    public static func store(loop: Loop) -> Store<State, Action> {
        return  .init(initialState: .init(loop: loop),
                      reducer: LoopCore())
    }
    
    var body: some ReducerProtocol<State, Action> {
        Reduce { state, action in
            switch action {
            // Automatic actions
            case .load:
                return .send(.loadAudioFile)
                
            case .loadAudioFile:
                ambientManager.load(id: state.id)
                guard let volumeSubject = ambientManager.volume(forID: state.id) else { return .none }
                guard let isPlayingSubject = ambientManager.playing(forID: state.id) else { return .none }
                return .merge(
                    volumeSubject
                        .catchToEffect()
                        .map { result in
                            if case let .success(volume) = result { return .setVolume(volume) }
                            else { return .none }
                        },
                    isPlayingSubject
                        .catchToEffect()
                        .map { result in
                            if case let .success(playing) = result { return .setIsPlaying(playing) }
                            else { return .none }
                        }
                )
                
            case .toggleSound:
                if state.isPlaying { return .send(.stopSound) }
                else { return .send(.startSound) }
                
            case .startSound:
                ambientManager.playSound(withID: state.id)
                return .send(.resetOffset)
                
            case .stopSound:
                ambientManager.stopSound(withID: state.id)
                return .send(.resetOffset)
                
            case .updateVolumeOffset(let volume):
                let volDiff = volume - (state.volumeOffset ?? 0.0)
                state.volumeOffset = volume
                ambientManager.set(
                    volume: (state.volume + volDiff).clamped(to: 0.0...1.0),
                    forID: state.id,
                    fadeDuration: 0.0)
                
            case .resetOffset:
                state.volumeOffset = nil
                
            case .setVolume(let volume):
                state.volume = volume
                
            case let .setIsPlaying(isPlaying):
                state.isPlaying = isPlaying
                
            // User Interactions
            case .soundButtonPressed:
                return .send(.toggleSound)
                
            case .soundButtonDragged(let size):
                return .send(.updateVolumeOffset(Float(size.width) / 200))
                
            case .soundButtonDragEnded(let size):
                return .send(.resetOffset)
                
            case .none: return .none
            }
            
            return .none
        }
    }
}
