//
//  ShotPlayer.swift
//  Ambience
//
//  Created by Ferdinand Göldner on 27.02.23.
//

import AVFAudio
import Combine

class ShotPlayer: AVAudioPlayer, Identifiable {
    var id: UUID
    var filename: String
    
    required init?(filename: String) {
        self.id = UUID()
        self.filename = filename
        
        guard let path = Bundle.main.path(forResource: "\(filename).mp3", ofType: nil) else { return nil }
        let url = URL(fileURLWithPath: path)

        do {
            completion = .init()
            try super.init(contentsOf: url, fileTypeHint: nil)
            self.volume = 1.0
            self.play()
            
            self
                .completion
                .send(completion: .finished)
        } catch {
            print(error)
            return nil
        }
    }
    
    private var completion: PassthroughSubject<Void, Never>
    public func completedSubject() -> AnyPublisher<Void, Never> {
        completion
            .delay(for: RunLoop.SchedulerTimeType.Stride(self.duration + 3), scheduler: RunLoop.main)
            .eraseToAnyPublisher()
    }
}

