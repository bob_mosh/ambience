//
//  Loops.swift
//  Ambience
//
//  Created by Ferdinand Göldner on 21.02.23.
//

import Foundation

extension Loop {
    static var bugs: Loop = .init(title: "Bugs", file: "Bugs")
    static var birds: Loop = .init(title: "Birds", file: "Birds")
    static var pebblesRollingInCavern: Loop = .init(title: "Pebbles Rolling in Cave", file: "Pebbles Rolling Cave")
    static var footsteps: Loop = .init(title: "Footsteps", file: "Footsteps")
    static var nighttimeBugs: Loop = .init(title: "Nighttime Bugs", file: "Nighttime Bugs")
    static var tavernNoises: Loop = .init(title: "Tavern Noises", file: "Tavern Noises")
    static var marketPeople: Loop = .init(title: "Market People", file: "Market People")
    static var caveMonsters: Loop = .init(title: "Cave Monsters", file: "Cave Monsters")
    static var caveAmbience: Loop = .init(title: "Cave Ambience", file: "Cave Ambience")
    static var seasideDocks: Loop = .init(title: "Seaside Docks", file: "Seaside Docks")
    static var campfireClose: Loop = .init(title: "Campfire Close", file: "Campfire Close")
    static var openField: Loop = .init(title: "Open Field", file: "Open Field")
    static var frozenRain: Loop = .init(title: "Frozen Rain", file: "Frozen Rain")
    static var outdoorRain: Loop = .init(title: "Outdoor Rain", file: "Outdoor Rain")
    static var rainOnTent: Loop = .init(title: "Soft Rain on Tent", file: "Soft Rain on Tent")
    static var softRain: Loop = .init(title: "Soft Rain", file: "Soft Rain")
    static var thunderstorm: Loop = .init(title: "Heavy Thunderstorm", file: "Heavy Thunderstorm")
    static var distantThunderstormWithLightRain: Loop = .init(title: "Thunderstorm w/ Light Rain", file: "Distant Thunderstorm with Light Rain")
    static var mediumThunderstorm: Loop = .init(title: "Medium Thunderstorm with Rain", file: "Medium Thunderstorm with Rain")
    static var roaringFire: Loop = .init(title: "Roaring Fire", file: "Roaring Fire")
    
    static var allLoops: [Loop] = [
        bugs,
        birds,
        pebblesRollingInCavern,
        footsteps,
        nighttimeBugs,
        tavernNoises,
        marketPeople,
        caveMonsters,
        caveAmbience,
        seasideDocks,
        campfireClose,
        openField,
        outdoorRain,
        frozenRain,
        rainOnTent,
        softRain,
        thunderstorm,
        distantThunderstormWithLightRain,
        mediumThunderstorm,
        roaringFire
    ]
}
