//
//  Shots.swift
//  Ambience
//
//  Created by Ferdinand Göldner on 27.02.23.
//

import Foundation

extension Shot {
    static var arrowDirt: Shot = .init(title: "Arrow Dirt", numberofVariants: 10)
    static var subImpact: Shot = .init(title: "Sub Impact", numberofVariants: 9)
    static var heavySwing: Shot = .init(title: "Heavy Swing", numberofVariants: 4)
    static var dragonGrowl: Shot = .init(title: "Dragon Growl", numberofVariants: 4)
    
    static var allShots: [Shot] = [
        arrowDirt,
        subImpact,
        heavySwing,
        dragonGrowl
    ]
}
