//
//  MainMenuView.swift
//  Ambience
//
//  Created by Ferdinand Göldner on 20.02.23.
//

import ComposableArchitecture
import SwiftUI

struct MainMenuView: View {
    // MARK: Lifecycle

    init(store: Store<MainMenuCore.State, MainMenuCore.Action>) {
        self.store = store
        self.viewStore = ViewStore(store)
    }
    
    // MARK: Internal

    let store: Store<MainMenuCore.State, MainMenuCore.Action>
    @ObservedObject var viewStore: ViewStore<MainMenuCore.State, MainMenuCore.Action>

    var body: some View {
        NavigationSplitView {
            List(
                selection: viewStore.binding(
                    get: { $0.navigatedScene },
                    send: { .navigateToScene($0) }
                )
            ) {
                ForEach(viewStore.scenes) { scene in
                    BoxedNavigationLink(
                        scene: scene,
                        isActive: viewStore.navigatedScene?.id == scene.id
                    )
                }
            }
            .navigationTitle("Scenes")
            .toolbar(content: {
                ToolbarItem {
                    Button {} label: {
                        Image(systemName: "plus")
                    }
                    .disabled(true)
                }
            })
            .listStyle(.plain)
            .padding(.horizontal, 8.0)
        } detail: { detailPage }
    }
    
    var detailPage: some View {
        IfLetStore(store.scope(
            state: \.navigatedScene,
            action: MainMenuCore.Action.navigatedScene
        )) { store in
            SceneView(store: store)
        } else: {
            Text("Select a Scene from the sidebar.")
                .foregroundColor(Color.primary.opacity(0.3))
        }
    }
}

struct MainMenuView_Preview: PreviewProvider {
    static var previews: some View {
        MainMenuView(store: MainMenuCore.store())
    }
}
