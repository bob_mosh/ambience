//
//  AmbientManager.swift
//  Ambience
//
//  Created by Ferdinand Göldner on 22.02.23.
//

import AVFAudio
import Combine
import Dependencies
import IdentifiedCollections

private enum AmbientManagerKey: DependencyKey {
    static let liveValue = AmbientManager.live
}

extension DependencyValues {
    var ambientManager: AmbientManager {
        get { self[AmbientManagerKey.self] }
        set { self[AmbientManagerKey.self] = newValue }
    }
}

class AmbientManager {
    // MARK: Lifecycle

    internal init() {
        initSessionDetails()
    }
    
    // MARK: Internal

    static var live = AmbientManager()

    func load(id: String) {
        guard loops[id: id] == nil else { return }
        guard let player = LoopPlayer(filename: id) else { return }
        loops[id: id] = player
    }
    
    func set(volume: Float, forID id: String, fadeDuration: TimeInterval = 2.0) {
        loops[id: id]?.setVolume(volume, fadeDuration: fadeDuration)
    }
    
    func load(preset: Preset) {
        stopAllSounds()
        
        for setting in preset.settings {
            let id = setting.loop.id
            set(volume: setting.volume, forID: id)
        }
    }
    
    func playSound(withID id: String) {
        if loops[id: id] == nil { load(id: id) }
        loops[id: id]?.play()
    }
    
    func stopSound(withID id: String) {
        loops[id: id]?.stop(animated: true)
    }

    // MARK: Private

    private var loops: IdentifiedArrayOf<LoopPlayer> = []

    private func stopAllSounds() {
        loops
            .filter { $0.isPlaying }
            .forEach { $0.stop(animated: true) }
    }
    
    private func initSessionDetails() {
        let session = AVAudioSession.sharedInstance()
        
        do {
            try session.setCategory(.playback, mode: .default,
                                    policy: .default, options: [.mixWithOthers, .allowAirPlay])
        } catch { print(error) }
    }
}

extension AmbientManager {
    func volume(forID id: String) -> CurrentValueSubject<Float, Never>? {
        return loops[id: id]?.volumeSubject
    }
    
    func playing(forID id: String) -> CurrentValueSubject<Bool, Never>? {
        return loops[id: id]?.isPlayingSubject
    }
}
