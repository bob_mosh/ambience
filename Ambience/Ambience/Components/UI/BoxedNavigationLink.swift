//
//  BoxedNavigationLink.swift
//  Ambience
//
//  Created by Ferdinand Göldner on 21.02.23.
//

import SwiftUI

struct BoxedNavigationLink: View {
    var scene: SceneCore.State
    var isActive: Bool

    var body: some View {
        NavigationLink(value: scene) {
            HStack {
                Text(scene.scene.title)
                    .fontWeight(.bold)

                Spacer()

                if isActive {
                    Image(systemName: "speaker.wave.2.fill")
                }
            }
            .padding([.vertical, .leading], 8)
            .foregroundColor(.primary)
        }
        .listRowBoxed(isActive: isActive)
    }
}
