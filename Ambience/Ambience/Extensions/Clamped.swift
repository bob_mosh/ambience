//
//  Clamped.swift
//  Ambience
//
//  Created by Ferdinand Göldner on 21.02.23.
//

import Foundation

extension Comparable {
    func clamped(to limits: ClosedRange<Self>) -> Self {
        return min(max(self, limits.lowerBound), limits.upperBound)
    }
}
